%define EXIT 60
%define STDOUT_DESCRIPTOR 1
%define COMMAND_WRITE 1
%define END_STRING 0xA
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax; we use it in a cycle after, first iteratig starting with zero
    mov r11, rdi
    .loop:
        cmp byte [rdi+rax], 0 ; while current symbol is not zero we continue increment rax
        je .exit
	inc rax
	jmp .loop
    .exit:
        mov rdi, r11
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
	push rdi
    call string_length; call previus function to get length of the string and put it in the rax
    pop rdi
	mov rdx, rax; move string lehgth from rax to rdx
    mov rsi, rdi; put pointer to the first symbol into rsi
    mov rax, COMMAND_WRITE
    mov rdi, STDOUT_DESCRIPTOR
    syscall
    mov rax, 0 
    ret

; Принимает код символа и выводит его в stdout
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, END_STRING

print_char:
    push rdi; need to do this for making a pointer to the char with rsp
    mov rsi, rsp; system take pointer to the string from rsi
    mov rdx, 1; length of one symbol is 1
    mov rdi, STDOUT_DESCRIPTOR
    mov rax, COMMAND_WRITE
    syscall
    pop rdi
    ret
    
    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10
    mov rdi, rsp
    sub rsp, 21; 2^64 - 20 numbers in 10-system + 1 byte for zero-terminator 
    dec rdi
    mov byte[rdi], 0
    .loop:
    	xor rdx, rdx
        div r8
	add dl, '0'
	dec rdi
	mov byte[rdi], dl
	cmp rax, 0
	jne .loop
    call print_string
    add rsp, 21
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    add rax, 0; just to set flags
    js .print_neg
    call print_uint
    jmp .exit
    .print_neg:
    	push rdi
	mov rdi, 0x2D
	call print_char
	pop rdi
	neg rdi
	call print_uint
    xor rax, rax
    .exit:
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx; rcx is index of symbol in our string
    .loop:
	mov r8b, byte[rdi + rcx]; in r8b we will store the current symbol of first string
    	mov r9b, byte[rsi + rcx]; in r9b we will store the current symbol of second string
	cmp r8b, r9b; check current symbols 
	jne .false
	cmp byte[rdi + rcx], 0; check if current symbol is zero. If zero that means the end of strings
	je .true
	inc rcx
	jmp .loop
    .true:
    	mov rax, 1; store the result in the rax
	ret
    .false:
    	xor rax, rax; store the result in the rax
        ret
		
    
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:  
	push r12
	push rbx
	push rbp
	mov rbx, rdi
	mov rbp, rsi

	.read:
	    call read_char
	    cmp rax, 0x20
	    je .enter
	    cmp rax, 0x9
	    je .enter
	    cmp rax, 0xA
	    je .enter
	    cmp rax, 0
	    je .end

	    cmp r12, rbp
	    jz .error
	    mov [rbx+r12], rax
	    inc r12
	    jmp .read
	.error:
	    xor rax, rax
	    mov rdx, r12
	    pop rbp
	    pop rbx
	    pop r12
	    ret
	.enter:
   	    cmp r12, 0
	    jz .read
	.end:
	    mov byte [rbx+r12], 0
            mov rdx, r12
            mov rax, rbx
    	    pop rbp
            pop rbx
            pop r12
            ret
parse_uint:
    xor rax, rax; rax as ussual for result
    xor rcx, rcx; for length
    mov r8, 10
    .loop:
    	movzx r9, byte[rdi+rcx]
	jmp .check_digit
    .main:
	mul r8
    	sub r9b, '0'
	add rax, r9
	inc rcx
	jmp .loop	
    .exit:
	mov rdx, rcx
    	ret	
    .check_digit:
    	cmp r9b, '0'
	jl .exit
	cmp r9b, '9'
	jg .exit
	jmp .main



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r9, r9
    mov r9b, byte[rdi]
    cmp r9b, '-'
    je .neg
    jmp parse_uint
    .neg:
    	inc rdi	
	call parse_uint
	inc rdx
	neg rax
	ret
  

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r8, r8
    xor r9, r9

    .loop
    	cmp r8, rdx; compare length of string and length of buffer
	jge .error

	mov r9b, byte[rdi+r8]
	cmp r9b, 0; if zero symbol that means the end of the string
	je .exit

	mov byte[rsi+r8], r9b
	inc r8
	jmp .loop

    .error:
    	mov rax, 0
	ret

    .exit:
    	mov byte[rsi+r8], 0
	inc r8
	mov rax, r8
	ret
